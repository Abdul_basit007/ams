<?
	include_once('employeeheader.php');
		if(!$emp['admin'])
		{
			header('Location: calendar.php');
		}
	$getempid = $_GET['id'];
	loginCheck();

	_time_diff();
	//$emp['admin'] = 1;

	$year = isset($_GET['y']) ? intval($_GET['y']) : date('Y');
	$month = isset($_GET['m']) ? intval($_GET['m']) : date('m');
	$table = $_GET['tv']+0;


	if($table && $table != 1){
		ECHO "Naughty! We are noting what you do. Don't do it again otherwise... <br><br><br><br> no problem, do it again to waste your time...";exit;
	}

	$current_ip = $emp['ip_addr'];
	if($emp['admin']){
		//var_dump($emp);
		$empIdg = isset($_GET['e']) ? intval($_GET['e']) : $emp['id'];
		$empId = $empIdg;
		$emps = array();
		$sql = "select id, decode(fullname, '$key') fullname,ip_addr from emp order by decode(fullname, '$key') asc";
		$rs = $db->query($sql);
		while($row = $rs->fetch_assoc()) $emps[] = $row;
	}
	else {
		$empId = $emp['id'];
		$current_ip = $emp['ip_addr'];
	}
	$months = array();
	$sql = "select distinct month(attDate) month from att where empId = $empId";
	$rs = $db->query($sql);
	while($rs && ($row = $rs->fetch_assoc())) $months[] = $row;
	$years = array();
	$sql = "select distinct year(attDate) year from att where empId = $empId";
	$rs = $db->query($sql);
	while($rs && ($row = $rs->fetch_assoc())) $years[] = $row;

	$holidays = array();
	$holidaysQuery = "SELECT holidayDate,  holidayTitle FROM holidays";
	$rs = $db->query($holidaysQuery);
	while($rs && ($row = $rs->fetch_assoc())){
		$holidays[] = $row;
	}
	$sql = "select id, inIp, outIp, attDate, decode(inTime,'$key') inTime, decode(outTime,'$key') outTime, decode(inComments,'$key') inComments, decode(outComments,'$key') outComments, empId
			from att
			where year(attDate) = '$year' and month(attDate) = '$month' and empId = $empId order by attDate";
	$rs = $db->query($sql);
	$atts = array();
	while($rs && ($row = $rs->fetch_assoc())){
		$dt = getdate(strtotime($row['attDate']));
		$row['weekday'] = $dt['weekday'];
		$atts[] = $row;
	}
?>

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
		<section class="content-header">
		  <h1>
			Employee Details

		   </h1>
		  <ol class="breadcrumb">
			<li><a href="<?= SITE_URL ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?= SITE_URL.'employees.php' ?>">Employees</a></li>
			<li class="active">Employee Details</li>
		  </ol>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#rewards" data-toggle="tab">Attendance</a></li>
							<li class=""><a href="#mapview" data-toggle="tab">Daily reports</a></li>
						</ul>
					<div class="tab-content">
              
            <!-- /.tab-pane -->
			<div class="active tab-pane" id="rewards">
                <form method=get>
				<div class="row">
						<? if($emps){ ?>
						<div class="col-md-3">
							<select name=e onchange="location.href='<?= SITE_URL.'detailemployee.php?tv='.$table.'&e=' ?>'+this.value" class="form-control">
										<? foreach($emps as $e){ 
											if($e['id'] == $empIdg){
												$current_ip = $e['ip_addr'];
									}
								?>
								<option value="<?= $e['id'] ?>" <? if($e['id'] == $empIdg) echo 'selected' ?>><?= $e['fullname'] ?></option>
								<? } ?>
							</select>
						</div>
						<? } ?>
						<? if($years){ ?>
						<div class="col-md-3">
							<select name=y onchange="this.form.submit()" class="form-control">
								<? foreach($years as $y){ ?>
								<option value="<?= $y['year'] ?>" <? if($y['year'] == $year) echo 'selected' ?>><?= $y['year'] ?></option>
								<? } ?>
							</select>
						</div>
						<? } ?>
						<? if($months){ ?>
							<div class="col-md-3">
							<select name=m onchange="this.form.submit()" class="form-control">
							<? foreach($months as $m){ ?>
							<option value="<?= $m['month'] ?>" <? if($m['month'] == $month) echo 'selected' ?>><?= $m['month'] ?></option>
							<? } ?>
							</select>
						</div>
						<? } ?>
							<input type=hidden name=tv value=<?= $table ?>>
				</div>
				
				
					<table class="table table-hover table-responsive table-bordered">
						
						<tr>
							<td  align="right" style="text-align:right; border: 0px solid yellow;" colspan=7>
								<? if ($table == 0) { ?>
									<a href="calendar.php?y=<?=$year?>&m=<?=$month?>&e=<?=$empId?>&tv=1" class="link btn btn-primary">Calendar View</a>
								<? } else { ?>
									<a href="calendar.php?y=<?=$year?>&m=<?=$month?>&e=<?=$empId?>&tv=0" class="link btn btn-primary">Table View</a>
								<? } ?>
							</td>
						</tr>
						<? if ($table == 1) { ?>
						<tr>
							<td colspan=6>
							<?= drawCalendar(intval($month),intval($year)) ?>
							</td>
						</tr>
						<? }elseif($atts){ ?>
						<tr  style="background-color:#222d32;color:white;font-size: 12px;">
							<th width=25%><?php //print $current_ip;?>Date</th>
							<th>Clock In Time</th>
							<? if($emp['admin']){ ?>
							<th>Clock Out Time</th>
							<? } else { ?>
							<th>Clock Out Time</th>
							<? } ?>
							<th>In-Office</th>
							<? if($emp['admin'] or true){ ?>
							<th>Clock In IP</th>
							<th>Clock Out IP</th>
							<? } ?>
							<!-- <th>Notes/Tasks</th> -->	
						</tr>
					   <?   foreach($atts as $att){ ?>
						<tr>
							<td>
								<?=  _date_format($att['attDate']); ?>
								<?php
								$start_time = $att['attDate'] . " " . _time_format($att['inTime']);
								$end_time   = $att['attDate'] . " " . _time_format($att['outTime']);
								?>
							</td>
							<td>
								<? if ($att['inTime']) { ?><?= _time_format($att['inTime']); ?><? } ?>
								<? if ($att['inComments']) { ?><a title="<?= $att['inComments']; ?>"><span class=com>(reason)</span></a><? } ?>
							</td>

							<td>
								<? if ($att['outTime']) { ?><?= _time_format($att['outTime']); ?><? } ?>
								<? if ($att['outComments']) { ?><a title="<?= $att['outComments']; ?>"><span class=com>(Mini Report)</span></a><? } ?>
							</td>
			
								<? if (!$att['inTime']) { ?>
							<td bgcolor="white"><font color="red">Full Day</font>
								<? } else if(!$att['outTime']) { ?>
							<td bgcolor="white"><font color="black">Half Day</font>
								<? } else if (($att['inTime']) && ($att['outTime'])) { ?>
							<td>	
								<?= _time_diff($start_time, $end_time); ?> hrs<? } ?>
							</td>

							<? if($emp['admin'] or true){ ?>
							<?php  if($current_ip != $att['inIp']) { ?>
							<td class="yell">
							<? } else { ?>
							<td>
							<? } ?>
							<?= $att['inIp'] ?></td>
							
							<?php  if($current_ip != $att['outIp']) { ?>
							<td class="yell">
							<? } else { ?>
							<td><? } ?>
							<?= $att['outIp'] ?></td>
							<? } ?>
							<!-- <td bgcolor="#e5e4f0">
								<a href="tasks.php?dt=<?= $att['attDate'] ?>&e=<?= $att['empId']?>" class=com>
									<font color=#261b95>
									<? if($att['weekday'] == 'Friday'){ ?>
									Daily Report
									<? }else{ ?>
									Daily Report
									<? } ?>
									</font>
								</a>
							</td> -->
						</tr>
					 <? }} ?>

					 <? if($emp['admin'] or true){ ?>
						<tr>
							 <td colspan=7 class="mrwhite">
									<!--<span class="mrnormal">
									Dear Friend, 
									
									<br>
									<span class="red">
									PakCyber working hours are 9 hours per day, 5 days a week.</span>
									<br>
									You are allowed half an hour <span class="blue">(compulsory break) between 1:30 PM to 2:00 PM</span>. 
									<br>
									You are encourged to take <span class="blue">5 to 15 minutes prayers break</span>. 
									<br>
									<span class="green">
									Depends on your performance, company would give you bonuses/gifts.
									</span>
									<br>
									<span class="redplus">
									In case you are not performing, having less working hours in-house, or in case complaints rose against your work, then it'd be accountable. 
									</span><br>
							Regards,
							<br>
							PakCyber Management		
									</span>--><br>
									<!--
									<span class="mrbold">Available Limits:</span>
									<br>
									<span>Before: 9:15 (Dude you are punctual and in time, nice to meet you! You are invited to "Tea Party" without any contribution.</span><br>
									<span class="green">Between: 9:16 to 9:30 (Green Zone - Allowed Limits 10 days) <br>You are little late so in green zone. You are allowed to be in green zone for 10 days. 11th day will be counted as penalty.</span><br>
									<span class="blue">Between: 9:31 to 9:45 (Blue Zone - Allowed Limits 5 days) <br>You are late but still you have intension to come early, so you are in blue zone. You are allowed to be in blue zone for 5 days. 6th day will be counted as penalty.</span><br>
									<span class="red">Between: 9:46 to 10:00 (Red Zone - Allowed Limits 3 days)<br>You are used to late frequently, bad  habit, you are now in red zone. You are allowed to be in red zone for 3 days. 4th day will be counted as penalty.</span><br>
									<span class="redplus">After: 10:AM (Red++ Zone - Allowed Limits 0 days)<br>You are the one we are finding. i.e. big contributor to our "Tea Party"... penalty will be decided by management at its own discretion, that would not be less then accumulative of all above in any case. </span><br>
									<span>Absent: You can take leave by informing management, no problem. If you are absent other then taking leave i.e. forgot to clock in, then same rules will be applied as mentioned in red++ zone.
									<span>All untold holidays will also be treated as a red++ mark.<br>
									<span class="mrbold">Contribution to "Tea Party":  RS: 100 Per Penalty Day!!! </span><br>
									-->
								 </td>
							</tr>
					 <? } ?>
					</table>
				</form>
               
            </div>
			<div class="tab-pane" id="mapview">
                <table class="table table-responsive table-stripped table-bordered">
					<tr style="background-color:#222d32;color:#b8c7ce;">
						<th style="width:300px;">Date</th>
						<th>Daily reports</th>
					</tr>
					 <?   foreach($atts as $att){ ?>
					<tr>
					<td>
						<?=  _date_format($att['attDate']); ?>
						<?php
						$start_time = $att['attDate'] . " " . _time_format($att['inTime']);
						$end_time   = $att['attDate'] . " " . _time_format($att['outTime']);
						?>
				    </td>
				
					<td><? echo $att['outComments']; ?></td>
					</tr>
					 <?}?>
				</table>
            </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
    </div>
			</div>
				
	</div>
		
		</section>
    <!-- Main content -->
   
    <div class="control-sidebar-bg"></div>
    
	<?php
	function timeSlot($dateIn, $timeIn){
		$formatDateIn = date("Y-m-d",strtotime($dateIn));
		$compareTimeIn = strtotime($formatDateIn . " " . $timeIn);

		$red   = strtotime($formatDateIn . " 09:46:00");
		$blue  = strtotime($formatDateIn . " 09:31:00");
		$green = strtotime($formatDateIn . " 09:16:00");

		if ($compareTimeIn - $red > 0)
			return "class=red";

		if ($compareTimeIn - $blue > 0)
			return "class=blue";

		if ($compareTimeIn - $green > 0)
			return "class=green";

		return "";
	}

	function getTotalDays($year, $month){
		switch($month){
			case 4:
			case 6:
			case 9:
			case 11:
				return 30;
			case 2:
				return $year % 4 ? 28 : 29;
			default:
				return 31;
		}
	}
	?>
	<!-- ./wrapper -->

	<!-- jQuery 2.2.3 -->
	<script src="admin_theme/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.6 -->
	<script src="admin_theme/bootstrap/js/bootstrap.min.js"></script>
	<!-- Morris.js charts -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="plugins/morris/morris.min.js"></script>
	<!-- Sparkline -->
	<script src="admin_theme/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script src="admin_theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="admin_theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- jQuery Knob Chart -->
	<script src="admin_theme/plugins/knob/jquery.knob.js"></script>
	<!-- daterangepicker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
	<script src="admin_theme/plugins/daterangepicker/daterangepicker.js"></script>
	<!-- datepicker -->
	<script src="admin_theme/plugins/datepicker/bootstrap-datepicker.js"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="admin_theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<!-- Slimscroll -->
	<script src="admin_theme/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="admin_theme/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="admin_theme/dist/js/app.min.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="admin_theme/dist/js/pages/dashboard.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="admin_theme/dist/js/demo.js"></script>
</body>
</html>
