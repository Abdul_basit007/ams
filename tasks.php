<?
	include_once('header.php');
	loginCheck();
	
	//$emp['admin'] = 1;
	if(isset($_GET['dt'])){
		$dt =  $_GET['dt'];
		$dtArr = explode('-', $dt);
		$dt = checkdate($dtArr[1],$dtArr[2],$dtArr[0]) ? $dt : $now;
	}
	else $dt = $now;
	
	if($emp['admin']){		
		$empIdg = isset($_GET['e']) ? $_GET['e']+0 : $emp['id'];		
		$empId = $empIdg;		
		$emps = array();
		$sql = "select id, decode(fullname, '$key') fullname from emp order by decode(fullname, '$key') asc";
		$rs = $db->query($sql);
		while($row = $rs->fetch_assoc()) $emps[] = $row;
	}
	else{
		$empId = $emp['id'];
	}
	
	$empName = db_scalar("select decode(fullname, '$key') fullname from emp where id = $empId");
	
	$sql = "select id from tasks where empId = $empId and tDate = '$dt'";

	$taskId = db_scalar($sql)+0;
	
	if($_POST['save']){
		//echo htmlentities($_POST['details']);
		$details = filter(strip_tags($_POST['details'], '<b><i><ul><ol><li><p><strong><em><span>'));		
		if($taskId) $sql = "update tasks set details = '$details' where id = $taskId";
		else $sql = "insert into tasks set empId = $empId, tDate = '$dt', details = '$details'";
		$db->query($sql);
		if(!$taskId) $taskId = $db->insert_id;
	}
	
	$task = array();
	$sql = "select details from tasks where id = $taskId";
	$details = db_scalar($sql);
	
	//admin tasks
	$sql = "select details, decode(fullname, '$key') adminName from admin_tasks inner join emp on emp.id = admin_tasks.adminId where empId = $empId and tDate = '$dt'";
	if($rs = $db->query($sql)){
		$admin_task = $rs->fetch_assoc();
	}
	
?>
<script type="text/javascript" src="tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$(function() {
		$('#details').tinymce({
			script_url : 'tiny_mce/tiny_mce.js',
			theme : "simple",			
			content_css : "tinymce.css",
		});
	});
</script>
<form method=post>
<table cellpadding=5 cellspacing=0 width=100% class=tabu>
	<tr>
		<th align=left><?= _date_format($dt) ?></th>
	</tr>
	
	<? if($emps){ ?>
	<tr>
		<td>
			<select name=e onchange="location.href='<?= SITE_URL.'tasks.php?e=' ?>'+this.value">
			<? foreach($emps as $e){ ?>
			<option value="<?= $e['id'] ?>" <? if($e['id'] == $empIdg) echo 'selected' ?>><?= $e['fullname'] ?></option>
			<? } ?>
			</select>
		</td>
	</tr>
	<? } ?>
	
	<? if($admin_task['details']){ ?>
	<tr>
		<th align=left>Daily Tasks assigned by <?= $admin_task['adminName'] ?></th>
	</tr>
	<tr>
		<td>			
			<?= nl2br($admin_task['details']) ?>
		</td>
	</tr>
	<tr><td><hr></td></tr>
	<? } ?>
	
	<tr>
		<th align=left>Daily Tasks Report by <?= $empName ?></th>
	</tr>
	<tr>
		<td>
			<textarea id=details name=details style="width:100%;height:500px" class="tinymce"><?= $details ?></textarea>
			<br>
			<? if(!($empIdg && $empIdg == $empId)){ ?>
			<input type=submit name=save value=Save>
			<? } ?>
		</td>
	</tr>
</table>
</form>
<?
	include_once('footer.php');
?>