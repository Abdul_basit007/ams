<?
	error_reporting(E_ALL & ~E_NOTICE);
	include_once('config.php');
	session_start();
	$db = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	$url = SITE_URL;
	$msg = '';
	$key = "dGFo";
	$now = date('Y-m-d');
	
	if(isset($_GET['logout'])){
		session_destroy();
		header('Location: '. $url); exit;
	}

	$emp = isset($_SESSION['emp']) ? $_SESSION['emp'] : array('id'=>0,'username'=>'','fullname'=>'');
		

	function loginCheck(){
		global $url;	

		if(!isset($_SESSION['emp'])){
			header('Location: '.$url); exit;
		}
	}
	
	function db_scalar($sql){
		global $db;
		$r = 0;
		
		if($rs = $db->query($sql)){
			$row = $rs->fetch_row();
			$r = $row[0];
		}
		
		return $r;
	}
	
	function _date_format($d){
		$d = is_int($d) ? $d : strtotime($d);
		return date('l, d F, Y', $d);
	}
	
	function _time_format($t){
		$t = is_int($t) ? $t : strtotime($t);
		return date('h:i:s a', $t);
	}
	
	function _time_diff($start_time="2013-10-20 08:10:00",$end_time="2013-10-20 09:10:01"){
	
		$difference = "DULL Person";
		
		if($start_time && $end_time){
			$date_a = new DateTime($start_time);
			$date_b = new DateTime($end_time);
			
			$date_a = $date_a->format("U");
			$date_b = $date_b->format("U");
			
			//$interval = date_diff($date_b,$date_a);
			$rv = ($date_b - $date_a);
			$interval = new DateInterval1($rv);
			
			$hours = $interval->h;
			$minutes = $interval->i;
		
			$hours = $hours + ($interval->d*24);

			$difference = $hours . ":" . $minutes;
			//echo $difference;
		}
		
		return $difference;
	}
	
	function filter($str){
		return get_magic_quotes_gpc() ? trim($str) : addslashes(trim($str)); 
	}
	function diff ($secondDate){
		$firstDateTimeStamp = $this->format("U");
		$secondDateTimeStamp = $secondDate->format("U");
		$rv = ($secondDateTimeStamp - $firstDateTimeStamp);
		$di = new DateInterval1($rv);
		return $di;
	}
	
	Class DateInterval1 {
		/* Properties */
		public $y = 0;
		public $m = 0;
		public $d = 0;
		public $h = 0;
		public $i = 0;
		public $s = 0;

		/* Methods */
		public function __construct ( $time_to_convert /** in seconds */) {
			$FULL_YEAR = 60*60*24*365.25;
			$FULL_MONTH = 60*60*24*(365.25/12);
			$FULL_DAY = 60*60*24;
			$FULL_HOUR = 60*60;
			$FULL_MINUTE = 60;
			$FULL_SECOND = 1;

	//        $time_to_convert = 176559;
			$seconds = 0;
			$minutes = 0;
			$hours = 0;
			$days = 0;
			$months = 0;
			$years = 0;

			while($time_to_convert >= $FULL_YEAR) {
				$years ++;
				$time_to_convert = $time_to_convert - $FULL_YEAR;
			}

			while($time_to_convert >= $FULL_MONTH) {
				$months ++;
				$time_to_convert = $time_to_convert - $FULL_MONTH;
			}

			while($time_to_convert >= $FULL_DAY) {
				$days ++;
				$time_to_convert = $time_to_convert - $FULL_DAY;
			}

			while($time_to_convert >= $FULL_HOUR) {
				$hours++;
				$time_to_convert = $time_to_convert - $FULL_HOUR;
			}

			while($time_to_convert >= $FULL_MINUTE) {
				$minutes++;
				$time_to_convert = $time_to_convert - $FULL_MINUTE;
			}

			$seconds = $time_to_convert; // remaining seconds
			$this->y = $years;
			$this->m = $months;
			$this->d = $days;
			$this->h = $hours;
			$this->i = $minutes;
			$this->s = $seconds;
		}
	}
	
	
?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>QuaidTech - QT Labs -  Attendance Management System</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="admin_theme/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->

  <!-- Theme style -->
  <link rel="stylesheet" href="admin_theme/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="admin_theme/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="admin_theme/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="admin_theme/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="admin_theme/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="admin_themeplugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="admin_theme/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="admin_theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">


  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="admin_theme/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="admin_theme/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="admin_theme/dist/css/skins/_all-skins.min.css">
  <style>
  .numberCircle {
    width: 25px;
    line-height: 25px;
    border-radius: 80%;
    text-align: center;
    font-size: 15px;
    color: white;
    background-color: #3c8dbc;
}
  </style>
</head>
<? if($emp['id'] > 0){ ?>
<body class="hold-transition skin-blue sidebar-mini">
<header class="main-header">
    <!-- Logo -->
    <a href="{{ route('home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>AMS</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>AMS</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
	<div class="user-panel">
			
			<div class="pull-left info">
                <p>Welcome <?= $emp['fullname'] ?></p>
                <a href="<?= SITE_URL.'?logout' ?>"><i class="fa fa-circle text-danger"></i> Logout</a>
			
            </div>
    </div>
       <ul class="sidebar-menu">
        <li class="header" style="color:blue;">MAIN NAVIGATION</li>
        <li><a href="<?= SITE_URL ?>"><i class="fa fa-book" style=""></i>Attendance</a></li>
		<li><a href="<?= SITE_URL.'calendar.php' ?>"><i class="fa fa-calendar"></i>Calendar</a></li>
		<? if($emp['admin']){ ?>
					<li><a href="<?= SITE_URL.'employees.php' ?>"><i class="fa fa-user"></i>Employee Management</a></li>
				<? } ?>
	    <li><a href="<?= SITE_URL.'holidays.php' ?>"><i class="fa fa-calendar"></i>Holidays Management</a></li>
		</ul>
	  
    </section>
    <!-- /.sidebar -->
  </aside>
 <div class="wrapper">
  <? } ?>