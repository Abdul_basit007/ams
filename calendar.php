<?
	include_once('employeeheader.php');
	loginCheck();

	_time_diff();
	//$emp['admin'] = 1;

	$year = isset($_GET['y']) ? intval($_GET['y']) : date('Y');
	$month = isset($_GET['m']) ? intval($_GET['m']) : date('m');
	$table = $_GET['tv']+0;


	if($table && $table != 1){
		ECHO "Naughty! We are noting what you do. Don't do it again otherwise... <br><br><br><br> no problem, do it again to waste your time...";exit;
	}

	$current_ip = $emp['ip_addr'];
	
	if($emp['admin']){
		//var_dump($emp);
		$empIdg = isset($_GET['e']) ? intval($_GET['e']) : $emp['id'];
		$empId = $empIdg;
		$emps = array();
		$sql = "select id, decode(fullname, '$key') fullname,ip_addr from emp order by decode(fullname, '$key') asc";
		$rs = $db->query($sql);
		while($row = $rs->fetch_assoc()) $emps[] = $row;
	}
	else {
		$empId = $emp['id'];
		$current_ip = $emp['ip_addr'];
	}
	$months = array();
	$sql = "select distinct month(attDate) month from att where empId = $empId";
	$rs = $db->query($sql);
	while($rs && ($row = $rs->fetch_assoc())) $months[] = $row;
	$years = array();
	$sql = "select distinct year(attDate) year from att where empId = $empId";
	$rs = $db->query($sql);
	while($rs && ($row = $rs->fetch_assoc())) $years[] = $row;

	$holidays = array();
	$holidaysQuery = "SELECT holidayDate,  holidayTitle FROM holidays";
	$rs = $db->query($holidaysQuery);
	while($rs && ($row = $rs->fetch_assoc())){
		$holidays[] = $row;
	}

	$sql = "select id, inIp, outIp, attDate, decode(inTime,'$key') inTime, decode(outTime,'$key') outTime, decode(inComments,'$key') inComments, decode(outComments,'$key') outComments, empId
			from att
			where year(attDate) = '$year' and month(attDate) = '$month' and empId = $empId order by attDate";
	$rs = $db->query($sql);
	$atts = array();
	while($rs && ($row = $rs->fetch_assoc())){
		$dt = getdate(strtotime($row['attDate']));
		$row['weekday'] = $dt['weekday'];
		$atts[] = $row;
	}
?>

<div class="content-wrapper">
<!-- Content Header (Page header) -->
	<section class="content-header" style="background-color: white">
	  <h1>
		 Calendar 

	   </h1>
	  <ol class="breadcrumb">
		<li><a href="<?= SITE_URL ?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active"> Calendar </li>
	  </ol>
	</section>
	<section class="content" style="background-color: white">	
		
<form method=get>
	<div class="row">
			<? if($emps){ ?>
			<div class="col-md-3">
				<select name=e onchange="location.href='<?= SITE_URL.'calendar.php?tv='.$table.'&e=' ?>'+this.value" class="form-control">
				<? foreach($emps as $e){ 
					if($e['id'] == $empIdg){
						$current_ip = $e['ip_addr'];
					}
				?>
				<option value="<?= $e['id'] ?>" <? if($e['id'] == $empIdg) echo 'selected' ?>><?= $e['fullname'] ?></option>
				<? } ?>
				</select>
			</div>
			<? } ?>
			<? if($years){ ?>
			<div class="col-md-3">
				<select name=y onchange="this.form.submit()" class="form-control">
				<? foreach($years as $y){ ?>
				<option value="<?= $y['year'] ?>" <? if($y['year'] == $year) echo 'selected' ?>><?= $y['year'] ?></option>
				<? } ?>
				</select>
			</div>
			<? } ?>
			<? if($months){ ?>
				<div class="col-md-3">
				<select name=m onchange="this.form.submit()" class="form-control">
				<? foreach($months as $m){ ?>
				<option value="<?= $m['month'] ?>" <? if($m['month'] == $month) echo 'selected' ?>><?= $m['month'] ?></option>
				<? } ?>
				</select>
			</div>
			<? } ?>
			<input type=hidden name=tv value=<?= $table ?>>
	</div>
	<br>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-2 col-sm-5 col-xs-5" style="padding-left:0px;">
			<? if ($table == 0) { ?>
						<a href="calendar.php?y=<?=$year?>&m=<?=$month?>&e=<?=$empId?>&tv=1" class="btn btn-block btn-primary"> Calendar View</a>
			<? } else { ?>
			<a href="calendar.php?y=<?=$year?>&m=<?=$month?>&e=<?=$empId?>&tv=0"  class="btn btn-block btn-primary">Table View</a>
			<? } ?>
			</div>
			<div class="col-md-2 col-sm-5 col-xs-5" style="float:right;">
			<?php 
			if($emp['admin'])
			{
			?>
			<a href="detailemployee.php?e=<?=$empId?>#mapview" class="btn btn-block btn-primary"> Daily Report</a>
			<?php 
			}
			?>
			</div>
		</div>
	</div>
	<br />
	<table class="table table-hover table-responsiv table-bordered">
	<? if ($table == 1) { ?>
	<tr>
		<td colspan=6>
		<?= drawCalendar(intval($month),intval($year)) ?>
		</td>
	</tr>

	<? }elseif($atts){ ?>

	<tr><td>
	<tr  style="background-color:#222d32;color:white;font-size: 12px;">
		<th width=25%><?php //print $current_ip;?>Date</th>
		<th>Clock In Time</th>
		<? if($emp['admin']){ ?>
		<th>Clock Out Time</th>
		<? } else { ?>
		<th>Clock Out Time</th>
		<? } ?>
		<th>In-Office</th>
		<? if($emp['admin'] or true){ ?>
		<th>Clock In IP</th>
		<th>Clock Out IP</th>
		<? } ?>
		<!-- <th>Notes/Tasks</th> -->
		
	</tr>
	<?   foreach($atts as $att){ ?>
	<tr>
		<td><?=  _date_format($att['attDate']); ?>
		<?php
		$start_time = $att['attDate'] . " " . _time_format($att['inTime']);
		$end_time   = $att['attDate'] . " " . _time_format($att['outTime']);
		?></td>

		<td>
			<? if ($att['inTime']) { ?><?= _time_format($att['inTime']); ?><? } ?>
			<? if ($att['inComments']) { ?><a title="<?= $att['inComments']; ?>"><span class=com>(reason)</span></a><? } ?>
		</td>

		<td>
			<? if ($att['outTime']) { ?><?= _time_format($att['outTime']); ?><? } ?>
			<? if ($att['outComments']) { ?><a title="<?= $att['outComments']; ?>"><span class=com>(Mini Report)</span></a><? } ?>
		</td>
		
			<? if (!$att['inTime']) { ?>
		<td bgcolor="white"><font color="red">Full Day</font>
			<? } else if(!$att['outTime']) { ?>
		<td bgcolor="white"><font color="black">Half Day</font>
			<? } else if (($att['inTime']) && ($att['outTime'])) { ?>
		<td>	
			<?= _time_diff($start_time, $end_time); ?> hrs<? } ?>
		</td>

		<? if($emp['admin'] or true){ ?>
		<?php  if($current_ip != $att['inIp']) { ?>
		<td class="yell">
		<? } else { ?>
		<td>
		<? } ?>
		<?= $att['inIp'] ?></td>
		
		<?php  if($current_ip != $att['outIp']) { ?>
		<td class="yell">
		<? } else { ?>
		<td><? } ?>
		<?= $att['outIp'] ?></td>
		<? } ?>
		
		
		<!-- <td bgcolor="#e5e4f0">
			<a href="tasks.php?dt=<?= $att['attDate'] ?>&e=<?= $att['empId']?>" class=com>
				<font color=#261b95>
				<? if($att['weekday'] == 'Friday'){ ?>
				Daily Report
				<? }else{ ?>
				Daily Report
				<? } ?>
				</font>
			</a>
		</td> -->
	</tr>
	<? }} ?>

	<? if($emp['admin'] or true){ ?>
	<tr>
		<td colspan=7 class="mrwhite">
		<!--<span class="mrnormal">
		Dear Friend, 
		
		<br>
		<span class="red">
		PakCyber working hours are 9 hours per day, 5 days a week.</span>
		<br>
		You are allowed half an hour <span class="blue">(compulsory break) between 1:30 PM to 2:00 PM</span>. 
		<br>
		You are encourged to take <span class="blue">5 to 15 minutes prayers break</span>. 
		<br>
		<span class="green">
		Depends on your performance, company would give you bonuses/gifts.
		</span>
		<br>
		<span class="redplus">
		In case you are not performing, having less working hours in-house, or in case complaints rose against your work, then it'd be accountable. 
		</span><br>
Regards,
<br>
PakCyber Management		
		</span>--><br>
		<!--
		<span class="mrbold">Available Limits:</span>
		<br>
		<span>Before: 9:15 (Dude you are punctual and in time, nice to meet you! You are invited to "Tea Party" without any contribution.</span><br>
		<span class="green">Between: 9:16 to 9:30 (Green Zone - Allowed Limits 10 days) <br>You are little late so in green zone. You are allowed to be in green zone for 10 days. 11th day will be counted as penalty.</span><br>
		<span class="blue">Between: 9:31 to 9:45 (Blue Zone - Allowed Limits 5 days) <br>You are late but still you have intension to come early, so you are in blue zone. You are allowed to be in blue zone for 5 days. 6th day will be counted as penalty.</span><br>
		<span class="red">Between: 9:46 to 10:00 (Red Zone - Allowed Limits 3 days)<br>You are used to late frequently, bad  habit, you are now in red zone. You are allowed to be in red zone for 3 days. 4th day will be counted as penalty.</span><br>
		<span class="redplus">After: 10:AM (Red++ Zone - Allowed Limits 0 days)<br>You are the one we are finding. i.e. big contributor to our "Tea Party"... penalty will be decided by management at its own discretion, that would not be less then accumulative of all above in any case. </span><br>
		<span>Absent: You can take leave by informing management, no problem. If you are absent other then taking leave i.e. forgot to clock in, then same rules will be applied as mentioned in red++ zone.
		<span>All untold holidays will also be treated as a red++ mark.<br>
		<span class="mrbold">Contribution to "Tea Party":  RS: 100 Per Penalty Day!!! </span><br>
		-->
		</td>
	</tr>
	<? } ?>
</table>
</form>
</section>
<?

include_once('footer.php');

function drawCalendar($month,$year){
	global $atts,$holidays;
   $matchYear  = $year;

   if($month < 10)
	$matchMonth = "0" . $month;
   else
	$matchMonth = $month;

   //print $matchYear . "---". $matchMonth;

  /* draw table */
  $calendar = '<table class="table table-hover table-responsive table-bordered">';

  /* table headings */
  $headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
  $calendar.= '<tr style="background-color:#222d32;color:white;font-size: 12px;"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

  /* days and weeks vars now ... */
  $running_day = date('w',mktime(0,0,0,$month,1,$year));
  $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
  $days_in_this_week = 1;
  $day_counter = 0;
  $dates_array = array();

  /* row for week one */
  $calendar.= '<tr class="calendar-row">';

  /* print "blank" days until the first of the current week */
  for($x = 0; $x < $running_day; $x++):
    $calendar.= '<td class="calendar-day-np">&nbsp;</td>';
    $days_in_this_week++;
  endfor;

  /* keep going with days.... */
  for($list_day = 1; $list_day <= $days_in_month; $list_day++):
    $calendar.= '<td class="calendar-day">';
      /* add in the day number */
      $calendar.= '<div class="day-number numberCircle">'.$list_day.'</div>';
	  if($list_day < 10)
		$matchDay = "0" . $list_day;
	  else
		$matchDay = $list_day;

	$matchDate = $matchYear . "-" . $matchMonth . "-" . $matchDay;

	/** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/

	$displayDetails = $displayIn = $displayOut = "";
	$showColor = "";
	$totalRec = count($atts);
	for($i=0; $i<=$totalRec; $i++){
		if($atts[$i]['attDate'] == $matchDate){
			$displayDetails = $atts[$i]['attDate'];
			//print $atts[$i]['inComments'];
			//print $atts[$i]['inTime'] . "---" . $atts[$i]['outTime'] . "<br>";

			if($atts[$i]['inTime'])
				$showColor = timeSlot($atts[$i]['attDate'], $atts[$i]['inTime']);

			if($atts[$i]['outTime'])
				$timeShowOut = date('h:i',strtotime($atts[$i]['outTime']));
			else
				$timeShowOut = "";
			
			if($atts[$i]['inTime'])
				$timeShowIn = date('h:i',strtotime($atts[$i]['inTime']));
			else
				$timeShowIn = "";
				
			if ($atts[$i]['inComments']) {
				$displayIn = "<a title=\"{$atts[$i]['inComments']}\"><u>" . date('h:i',strtotime($atts[$i]['inTime']))  .  "</u></a>";
			}
			else{
				$displayIn = $timeShowIn;
			}

			if ($atts[$i]['outComments']) {
				$displayOut = "<a title=\"{$atts[$i]['outComments']}\"><u>" . $dateShow .  "</u></a>";
			}
			else{
				$displayOut = $timeShowOut;
			}
			
			$displayDetails =  $displayIn . " - " . $displayOut . "<br>"  ;
			break;
		}
		else{
			if($days_in_this_week == 7 or $days_in_this_week == 1)
				$displayDetails = "--";
			else
				if ($matchDate <= date("Y-m-d")){
					$totalHolidays = count($holidays);
					$displayDetails = "<font style='font-size: 13px;' color='orange'><b>ABSENT</b></font>";
					for($j=0; $j<=$totalHolidays;$j++) {
						if($holidays[$j]['holidayDate'] == $matchDate){
								$displayDetails = "<font color='orange'><b>Holiday <br> {$holidays[$j]['holidayTitle'] }</b></font>";
						}
					}
				}
				else
					$displayDetails = "";
		}
	}
	
    $calendar.= str_repeat("<p><span {$showColor}>".$displayDetails . '</span></p>',1);

    $calendar.= '</td>';
    if($running_day == 6):
      $calendar.= '</tr>';
      if(($day_counter+1) != $days_in_month):
        $calendar.= '<tr class="calendar-row">';
      endif;
      $running_day = -1;
      $days_in_this_week = 0;
    endif;
    $days_in_this_week++; $running_day++; $day_counter++;
  endfor;

  /* finish the rest of the days in the week */
  if($days_in_this_week < 8):
    for($x = 1; $x <= (8 - $days_in_this_week); $x++):
      $calendar.= '<td class="calendar-day-np">&nbsp;</td>';
    endfor;
  endif;

  /* final row */
  $calendar.= '</tr>';

  /* end the table */
  $calendar.= '</table>';

  /* all done, return result */
  return $calendar;
}

function timeSlot($dateIn, $timeIn){
	$formatDateIn = date("Y-m-d",strtotime($dateIn));
	$compareTimeIn = strtotime($formatDateIn . " " . $timeIn);

	$red   = strtotime($formatDateIn . " 09:46:00");
	$blue  = strtotime($formatDateIn . " 09:31:00");
	$green = strtotime($formatDateIn . " 09:16:00");

	if ($compareTimeIn - $red > 0)
		return "class=red";

	if ($compareTimeIn - $blue > 0)
		return "class=blue";

	if ($compareTimeIn - $green > 0)
		return "class=green";

	return "";
}

function getTotalDays($year, $month){
	switch($month){
		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
		case 2:
			return $year % 4 ? 28 : 29;
		default:
			return 31;
	}
}
?>
