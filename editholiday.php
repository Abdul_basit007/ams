<?php
	include_once('employeeheader.php');
		if(!$emp['admin'])
		{
			header('Location: calendar.php');
		}
?>
<?php
			if(isset($_POST['editsub']))
			{
				
				$holidayTitle =$_POST['holidayTitle'] ;
				$holidayDate =$_POST['holidayDate'] ;
				$chsngeDate = date('Y-m-d', strtotime(str_replace('-', '/', $holidayDate)));
				
				$holiday = $_GET['id'];
				$sql = "update holidays set holidayTitle = '".$holidayTitle."',
				                           holidayDate ='".$chsngeDate."'   where id = '".$holiday."'";
				$rs=$db->query($sql);
					 if($rs)
						{
							header('Location: holidays.php?msg=update');
						} 
			} 

		if(isset($_GET['id']))
		{
			$holidayid = $_GET['id'];
			
			$sql = "select * from holidays WHERE id='".$holidayid."'";
			$rs = $db->query($sql);
			$arr=array();
		}
?>
<style>
.errorstar{color:#dd4b39;}
</style>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Holiday
       </h1>
      <ol class="breadcrumb">
        <li><a href="<?= SITE_URL ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?= SITE_URL.'holidays.php' ?>">Holidays</a></li>
        <li class="active">Edit Holiday</li>
      </ol>
    </section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Holiday</h3>
                </div>
				
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" role="form" method="POST" action="">
                  
					 <?php while($row=$rs->fetch_assoc()){?>
						      
                    <div class="box-body">
                        <div class="form-group">
                            <label for="holidayTitle" class="col-sm-2 control-label">Title<span class="errorstar">*</span></label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" placeholder="" id="holidayTitle" name="holidayTitle" required
								value="<?php echo $row['holidayTitle']; ?>">
                            </div>
                        </div>
                    </div>
					<div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Date <span class="errorstar">*</span></label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" placeholder="User Name" id="holidayDate" name="holidayDate" required value="<?php echo date('Y-m-d', strtotime(str_replace('-', '/', $row['holidayDate'])));?>">
                            </div>
                        </div>
                    </div>
					 <?php } ?>
                   <div class="box-footer">
                        
                        <button type="submit" name="editsub" class="btn btn-info pull-right">Update</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
    <!-- Main content -->
   
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="admin_theme/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="admin_theme/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="admin_theme/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="admin_theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="admin_theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="admin_theme/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="admin_theme/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="admin_theme/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="admin_theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="admin_theme/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="admin_theme/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="admin_theme/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="admin_theme/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="admin_theme/dist/js/demo.js"></script>
</body>
</html>
